public with sharing class TestPage_transactionCX {
    String JSONContent;
    private final Account acct;
    public JSONParser parser ;
    public Map <String,List <valuesWrapperClass>> valuesMap {get;set;}
    public TestPage_transactionCX() {
        JSONContent = '[{"firstName": "Jitendra","lastName" : "Zaa","age" : 26,"address" :{"streetAddress": "21 2nd Street","city" : "Nagpur","state" : "MH","postalCode" : "400008"},"phoneNumber":[{"Mobile": "212 555-1234"},{"Home": "646 555-4567"}]},{"firstName": "Justin","lastName" : "Lester","age" : 31,"address" :{"streetAddress": "5th ave","city" : "Makati","state" : "MM","postalCode" : "0012"},"phoneNumber":[{"Mobile": "555 434-66"},{"Home": "711 2345"}]}]';
        decode();
    }

    public TestPage_transactionCX(ApexPages.StandardController controller) {
        this.acct = (Account)controller.getRecord();
        Account currentAccountRecord = [Select id, POC_Json_String__c FROM Account WHERE id =:acct.id];
        JSONContent  = currentAccountRecord .POC_Json_String__c;
        
        if(String.isNotBlank(JSONContent)){
            try{
                decode();
            }catch (exception  e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Unable to parse JSON respose.');
                ApexPages.addMessage(myMsg);
                valuesMap = new Map <String,List <valuesWrapperClass>>();
            }
        }
    }
    
    public void decode(){
        parser = JSON.createParser(JSONContent);
        valuesWrapperClass tempValues= new valuesWrapperClass ();
        List <valuesWrapperClass> tempValuesList = new List<valuesWrapperClass>();
        valuesMap = new Map <String,List <valuesWrapperClass>>();
        Integer objectLevel = 0;
        
        String prevToken ='';
        String prevTokenText = '';
        String objectName = '';
        while (parser.nextToken() != null) {
            if(String.valueof(parser.getCurrentToken())=='START_OBJECT'){
                objectLevel++;
                system.debug('&&objectlevel:'+objectLevel);
            }else if(String.valueof(parser.getCurrentToken())=='END_OBJECT'){
                /**if(objectLevel>1){
                    tempValues.fieldValue = '-- 2nd level object --';
                    tempValuesList.add(tempValues);
                    system.debug('++'+parser.getText());
                }**/
                objectLevel--;
                system.debug('&&objectlevel back to:'+objectLevel);
            }
            system.debug('***prevToken:'+prevToken+' prevTokenText:'+prevTokenText );
            if(objectLevel==2 && prevToken == 'FIELD_NAME' && String.valueof(parser.getCurrentToken())=='START_OBJECT' ){
                objectName = prevTokenText;
            }
            else{
                if(String.valueof(parser.getCurrentToken())=='FIELD_NAME'){
                    tempValues = new valuesWrapperClass ();
                    tempValues.fieldName = parser.getText();
                    system.debug('**'+parser.getText());
                } else if(String.valueof(parser.getCurrentToken()).contains('VALUE_')){
                    tempValues.fieldValue = parser.getText();
                    tempValuesList.add(tempValues);
                    system.debug('++'+parser.getText());
                }
            }
            
            
            if(objectLevel==1 && String.valueof(parser.getCurrentToken())=='END_OBJECT'){
                valuesMap.put(objectName,tempValuesList);
                system.debug('$$$tempValuesList:'+tempValuesList);
                tempValuesList = new List<valuesWrapperClass>();
            }
            
            prevToken = String.valueof(parser.getCurrentToken());
            prevTokenText = parser.getText();
        }
        system.debug('$$$valuesMap:'+valuesMap);
    }
   
    public class valuesWrapperClass{
        public String fieldName {get;set;}
        public String fieldValue {get;set;}
        
        public valuesWrapperClass(){
            fieldname = '';
            fieldValue = '';
        }
    }
}